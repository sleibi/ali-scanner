package aliscanner.tqniyati.com.scanner;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.Result;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class BarcodeActivity extends Activity implements ZXingScannerView.ResultHandler {
    private ZXingScannerView mScannerView;
    LinearLayout qrshower,wrapper,productPanel;
    BarcodeActivity b;
    ImageView pimage;
    TextView pshop,pcolor,pstock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_barcode);
        b=this;
        qrshower=(LinearLayout) findViewById(R.id.qrshower);
        wrapper=(LinearLayout) findViewById(R.id.wrapper);
        productPanel=(LinearLayout) findViewById(R.id.productPanel);
        pstock = (TextView) findViewById(R.id.pstock);
        pshop = (TextView) findViewById(R.id.pshop);
        pcolor = (TextView) findViewById(R.id.pcolor);
        pimage = (ImageView) findViewById(R.id.pimage);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                QrScanner2();
            }
        });

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {


            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CAMERA)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA},
                        00000000);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }

    }

    public void QrScanner(View view){


        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        setContentView(mScannerView);

        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();         // Start camera

    }
    public void QrScanner2(){

        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        //setContentView(mScannerView);
        wrapper.setVisibility(View.VISIBLE);
        qrshower.addView(mScannerView);
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();         // Start camera

    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            mScannerView.stopCamera();
        }catch(Exception e){}// Stop camera on pause
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
        new fetchAPI(((Context) b),rawResult.getText()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        //Toast.makeText(getApplicationContext(),rawResult.getText(),Toast.LENGTH_LONG).show();


        // If you would like to resume scanning, call this method below:
        // mScannerView.resumeCameraPreview(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 00000000: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public  void  back(View v){
        qrshower.removeAllViews();
        productPanel.setVisibility(View.GONE);
        wrapper.setVisibility(View.VISIBLE);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                QrScanner2();
            }
        });

    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private class fetchAPI extends AsyncTask<String, Void, String> {
        ProgressDialog dialog;
        Context c;
        String barcode;
        public fetchAPI(Context c,String barcode){
            this.c=c;
            this.barcode=barcode;

        }
        protected void onPreExecute() {
            dialog = ProgressDialog.show(c, "",
                    "Loading", true);

        }

        protected String doInBackground(String... params) {
            String result="";
            // Connect to webservice
            String url= "http://alifouad91.com/scan/api/ws.php?type=stock&barcode="+barcode;

            try {
                result=getResponseText(url);
            }catch (Exception e){
                result="internet";
            }

            return result;
        }

        protected void onProgressUpdate(Void... values) {

        }

        protected void onPostExecute(String result) {
            dialog.dismiss();
            if(result.equals("internet")){
                android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(c).create();
                alertDialog.setTitle("Fail");
                alertDialog.setMessage("Please Check Internet Connection");
                alertDialog.setButton(android.support.v7.app.AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {}
                });
                alertDialog.show();
            }
            else if(result.equals("error")){
                Toast.makeText(c,"Product not found.",Toast.LENGTH_LONG).show();

            }else{

                qrshower.removeAllViews();
                wrapper.setVisibility(View.GONE);
                productPanel.setVisibility(View.VISIBLE);
                try {
                    JSONObject a=new JSONObject(result);

                    pstock.setText(a.getString("STOCK_QUANTITY")+" Available in stock");
                    pshop.setText("Shop name: "+a.getString("SHOPNAME"));
                    pcolor.setText("Color Code"+a.getString("COLOR_Code"));
                    new GetImage(pimage,"http://alifouad91.com/scan/"+a.getString("product_image")).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }catch (Exception e){
                    Toast.makeText(c,e.getMessage(),Toast.LENGTH_LONG).show();
                };

            }



        }
        private String getResponseText(String stringUrl) throws IOException
        {
            StringBuilder response  = new StringBuilder();

            URL url = new URL(stringUrl);
            HttpURLConnection httpconn = (HttpURLConnection)url.openConnection();

            if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK)
            {
                BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()),8192);
                String strLine = null;
                while ((strLine = input.readLine()) != null)
                {
                    response.append(strLine);
                }
                input.close();
            }
            return response.toString();
        }

    }
    private class GetImage extends AsyncTask<String, Void, String> {
        ProgressDialog dialog;
        ImageView image;
        String name;
        Bitmap bmp;
        public GetImage(ImageView image,String url){
            this.image=image;
            this.name=url;

        }
        protected void onPreExecute() {
            //image.setImageDrawable(getResources().getDrawable(R.drawable.loading));
        }
        protected String doInBackground(String... params) {
            String result="";
            // Connect to webservice


            try {
                URL url = new URL(name);
                bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


            }catch (Exception e){

            }

            return null;
        }


        protected void onPostExecute(String result) {
            image.setImageBitmap(getResizedBitmap(bmp,200));
        }

    }


    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 0) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }


        return getRoundedCornerBitmap(Bitmap.createScaledBitmap(image, width, height, true),9);
    }
    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }


}
