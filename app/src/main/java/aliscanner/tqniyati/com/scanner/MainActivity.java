package aliscanner.tqniyati.com.scanner;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;

import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;

import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class MainActivity extends AppCompatActivity {
    EditText username;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        username= (EditText) findViewById(R.id.username);
    }

    public void Login(View v){
        new fetchAPI(this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private class fetchAPI extends AsyncTask<String, Void, String> {
        ProgressDialog dialog;
        Context c;
        String usernames;
        public fetchAPI(Context c){
            this.c=c;

        }
        protected void onPreExecute() {
            dialog = ProgressDialog.show(c, "",
                    "Loading", true);
            usernames=username.getText().toString();
        }

        protected String doInBackground(String... params) {
            String result="";
            // Connect to webservice
            String url= "http://alifouad91.com/scan/api/ws.php?type=login&username="+usernames;

            try {
                result=getResponseText(url);
            }catch (Exception e){
                result="internet";
            }

            return result;
        }

        protected void onProgressUpdate(Void... values) {

        }

        protected void onPostExecute(String result) {
            dialog.dismiss();
            if(result.equals("internet")){
                AlertDialog alertDialog = new AlertDialog.Builder(c).create();
                alertDialog.setTitle("Fail");
                alertDialog.setMessage("Please Check Internet Connection");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {}
                });
                alertDialog.show();
            }
            else if(result.equals("success")){
                finish();
                Intent i = new Intent(c,BarcodeActivity.class);
                startActivity(i);

            }else{

            }

        }
        private String getResponseText(String stringUrl) throws IOException
        {
            StringBuilder response  = new StringBuilder();

            URL url = new URL(stringUrl);
            HttpURLConnection httpconn = (HttpURLConnection)url.openConnection();

            if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK)
            {
                BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()),8192);
                String strLine = null;
                while ((strLine = input.readLine()) != null)
                {
                    response.append(strLine);
                }
                input.close();
            }
            return response.toString();
        }

    }
    private class GetImage extends AsyncTask<String, Void, String> {
        ProgressDialog dialog;
        ImageView image;
        String name;
        Bitmap bmp;
        public GetImage(ImageView image,String url){
            this.image=image;
            this.name=url;

        }
        protected void onPreExecute() {
            //image.setImageDrawable(getResources().getDrawable(R.drawable.loading));
        }
        protected String doInBackground(String... params) {
            String result="";
            // Connect to webservice


            try {
                URL url = new URL(name);
                bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


            }catch (Exception e){

            }

            return null;
        }


        protected void onPostExecute(String result) {
            image.setImageBitmap(getResizedBitmap(bmp,200));
        }

    }


    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 0) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }


        return getRoundedCornerBitmap(Bitmap.createScaledBitmap(image, width, height, true),9);
    }
    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }
}
